# Sistema NetBank

NetBank é um sistema para gerenciar contas bancárias online.

Principais requisitos para o Módulo do Gerente:

a) O sistema deverá permitir que o gerente consulte as informações dos clientes da empresa. O gerente poderá visualizar as seguintes informações dos clientes: nome completo, login e data de nascimento.

b) O sistema deverá permitir que o gerente consulte as informações das contas bancárias online. O gerente poderá visualizar as seguintes informações das contas: número, titular e saldo.

c) O sistema deverá permitir que o gerente execute a transferência de valor de uma conta (de origem) para a outra (de destino). A transferência só poderá ser realizada se o valor da transferência for de, no máximo, R$ 10.000,00. Além disso, o valor da transferência não pode exceder o saldo da conta de origem. 


## 1. Diagrama de casos de uso

```plantuml
@startuml
left to right direction
actor Gerente
rectangle "Sistema NetBank" {
    Gerente -- (Consultar informações do cliente)
    Gerente -- (Consultar informações da conta)
    Gerente -- (Realizar transferência)
}
@enduml
```

## 2. Descrições dos casos de uso

### 2.1. Consultar informações do cliente (CDU001)

(em elaboração)

### 2.2. Consultar informações da conta (CDU002)

(em elaboração)

### 2.3. Realizar transferência (CDU003)

**Resumo:** O gerente deseja realizar uma transferência de valor de uma conta (de origem) para outra (de destino). Para executar esta operação, ele deverá informar a conta de origem, a conta de destino e o valor a ser transferido.

**Ator principal:** Gerente

**Pré-condições:** O gerente está autenticado no sistema e tem acesso ao menu de transferências.

**Pós-condições:** A transferência está registrada no sistema. O valor da transferência foi debitado da conta de origem. O valor da transferência foi creditado na conta de destino.

#### Fluxo principal

1. O gerente escolhe a operação de criar uma nova transferência.
2. O sistema solicita os dados para a transferência: conta de origem, conta de destino e valor.
3. O gerente fornece os dados solicitados.
4. O sistema verifica que a transferência pode ser realizada.
5. O sistema debita o valor da conta de origem e credita o mesmo valor na conta de destino.
6. O sistema informa que a transferência foi realizada.

#### Fluxos de exceção

##### Passo 4a (o valor de transferência excede o limite):

* O sistema verifica que o valor de transferência excede o limite de R$ 10.000,00 e informa que não é possível realizar a transferência. O caso de uso retorna para o passo 2 do fluxo principal.

##### Passo 4b (o valor de transferência excede o saldo da conta de origem):

* O sistema verifica que o valor de transferência excede o saldo da conta de origem e informa que não é possível realizar a transferência. O caso de uso retorna para o passo 2 do fluxo principal.

##### Passo 4c (a conta de origem e a conta de destino são as mesmas):

* O sistema verifica que a conta de origem e a conta de destino são as mesmas e informa que não é possível realizar a transferência. O caso de uso retorna para o passo 2 do fluxo principal.

