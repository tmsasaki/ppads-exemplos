# Sistema MovieReview

MovieReview é uma rede social onde seus membros podem avaliar filmes e obter recomendações com base nas avaliações que já fez. 


## 1. Diagrama de casos de uso

```plantuml
@startuml
left to right direction
actor Membro
actor Visitante
rectangle "Sistema MovieReview" {
    Visitante -- (Tornar-se membro)
    Membro -- (Acessar funções restritas)
    Membro -- (Avaliar filme)
    Membro -- (Obter recomendações)
}
@enduml
```

## 2. Descrições dos casos de uso

### 2.1. Tornar-se membro (CDU001)

**Resumo:** Um internauta que visita a página inicial da rede social tem a opção de tornar-se um membro. Tornando-se um membro, ele poderá avaliar filmes e obter recomendações de filmes.

**Ator principal:** Visitante

**Pré-condições:** Nenhuma

**Pós-condições:** O visitante está registrado como membro e consegue acessar as funções restritas aos membros da rede social.

#### Fluxo principal

1. O visitante seleciona a opção de tornar-se um membro.
2. O sistema solicita os dados iniciais para o registro: nome completo e *username* que gostaria de utilizar para acessar o sistema.
3. O visitante fornece os dados solicitados.
4. O sistema verifica que o *username* informado está disponível.
5. O sistema solicita que o visitante escolha uma senha com o mínimo de 10 caracteres, fornecendo-a duas vezes para confirmação.
6. O sistema verifica que a senha atende os critérios apresentados e que as duas entradas da senha coincidem.
7. O sistema registra o visitante como membro.
8. O sistema informa o visitante de que ele está registrado e pode acessar o sistema.

#### Fluxos de exceção

##### Passo 4 (*username* não está disponível):

* O sistema verifica que o *username* escolhido já está sendo utilizado e solicita que o visitante escolha outro. O caso de uso retorna para o passo 3 do fluxo principal.

##### Passo 6 (senha não tem o mínimo de 10 caracteres):

* O sistema verifica que a senha escolhida não atende os critérios apresentados e solicita que o visitante escolha outra senha. O caso de uso retorna para o passo 5 do fluxo principal.

##### Passo 6 (as duas entradas da senha não conferem):

* O sistema verifica que as duas entradas da senha não conferem e solicita que o visitante preencha novamente os campos com a senha desejada. O caso de uso retorna para o passo 5 do fluxo principal.

### 2.2. Acessar funções restritas (CDU002)

**Resumo:** Para avaliar filmes e obter recomendações de filmes, que são funções restritas aos membros da rede social, o membro deve se autenticar.

**Ator principal:** Membro

**Pré-condições:** O usuário já deve ser um membro registrado na rede social.

**Pós-condições:** O membro terá acesso às funções que são restritas apenas aos membros (avaliar filmes e obter recomendações de filmes).

#### Fluxo principal

1. O membro seleciona a opção de entrar na rede social.
2. O sistema solicita que o membro forneça seu *username* e sua senha.
3. O membro fornece seu *username* e sua senha.
4. O sistema verifica que o *username* e a senha correspondem às informações de um de seus usuários registrados.
5. O sistema verifica que a senha atende os critérios apresentados e que as duas entradas da senha coincidem.
6. O sistema inicia uma sessão e apresenta as opções de avaliar filmes e de obter recomendações.

#### Fluxos de exceção

##### Passo 4 (*username* e/ou senha inválidos):

* O sistema verifica que o *username* e a senha NÃO correspondem às informações de um de seus usuários registrados e solicita que o usuário entre novamente as informações. O caso de uso retorna para o passo 2 do fluxo principal.

### 2.3. Avaliar filme (CDU003)

**Resumo:** O membro irá entrar a avaliação de um filme que ele assistiu.

**Ator principal:** Membro

**Pré-condições:** O usuário é um membro registrado na rede social e está autenticado no sistema.

**Pós-condições:** A avaliação é registrada no sistema e passa a ser levada em consideração ao oferecer recomendações para o membro.

#### Fluxo principal

1. O membro seleciona a opção de avaliar um filme.
2. O sistema solicita algumas palavras, que podem fazer parte do nome do filme ou ser parte do nome do diretor, para buscar o filme que o membro deseja avaliar.
3. O membro fornece as palavras para a busca.
4. O sistema pesquisa pelos filmes que satisfazem o critério da busca.
5. O sistema apresenta uma lista dos filmes que satisfazem o critério
6. O membro seleciona o filme da lista que ele deseja avaliar.
7. O sistema apresenta as informações do filme (nome do filme, diretor, país, ano de lançamento) e solicita as informações da avaliação (nota de 1 a 5, comentário de até 1000 caracteres).
8. O membro fornece as informações da avaliação.
9. O sistema cria e registra a avaliação, e passa a levá-la em consideração ao oferecer recomendações para o membro.

#### Fluxos alternativos

##### Passo 5 (nenhum filme satisfaz o critério de busca):

* O sistema verifica que nenhum filme satisfaz o critério de busca e sugere que o membro faça uma nova busca com outras palavras. O caso de uso retorna para o passo 2 do fluxo principal.

### 2.4. Obter recomendações (CDU004)

**Resumo:** O membro deseja que o sistema ofereça recomendações de filmes com base nas avaliações que ele já fez. 

**Ator principal:** Membro

**Pré-condições:** O usuário é um membro registrado na rede social e está autenticado no sistema.

**Pós-condições:** O membro recebe recomendações de filmes que ele ainda não avaliou e que foram bem avaliados por outros membros que têm um perfil semelhante ao dele.

#### Fluxo principal

1. O membro seleciona a opção de obter recomendações.
2. O sistema consulta as recomendações geradas para este membro.
3. O sistema apresenta a lista das recomendações para o membro, onde cada recomendação apresenta os dados do filme e um índice de recomendação (com valor na faixa de 0% a 100%).

## 3. Modelo de domínio
```plantuml
@startuml
class Membro
class Filme
class Recomendacao
Membro : nomeCompleto
Membro : username
Membro : senha
class Avaliacao
Filme : nome
Filme : diretor
Filme : pais
Filme : anoLancamento
Avaliacao : nota
Avaliacao : comentario
Recomendacao : indice

Membro "*" - "*" Filme : avalia >
(Membro, Filme) .. Avaliacao
Membro "1" -up- "*" Recomendacao : obtém >
Recomendacao "*" - "1" Filme : dada para >
@enduml
```

## 4. Diagramas de sequência

### 4.1. Tornar-se membro (CDU001)
(elaborar)

### 4.2 Acessar funções restritas (CDU002)
(elaborar)

### 4.3. Avaliar filme (CDU003)

```plantuml
@startuml
hide footbox
actor Membro
boundary "__membroBoundary__" as mb
control "__avaliarFilmeControl__" as afc
entity "__filmesCatalog__" as fc
entity "__avaliacoesCatalog__" as ac

Membro -> mb : 1. seleciona a opção de avaliar um filme
activate mb
mb -> mb : 2. apresenta solicitação de palavras
activate mb
deactivate mb 
deactivate mb 

Membro -> mb : 3. fornece as palavras para a busca
activate mb
mb -> afc : buscarFilmesQueContem(palavras)
activate afc
afc -> fc : buscarPeloNomeContendo(palavras)
activate fc
afc <<-- fc : filmesResultantesDaBusca1
deactivate fc
afc -> fc : buscarPeloDiretorContendo(palavras)
activate fc
afc <<-- fc : filmesResultantesDaBusca2
deactivate fc
mb <<-- afc : filmesResultantesDaBusca
deactivate afc
mb -> mb: 5. apresenta a lista dos filmes
activate mb
deactivate mb
deactivate mb

Membro -> mb : 6. seleciona o filme que deseja avaliar
activate mb
mb -> mb: 7. apresenta detalhes do filme e\nsolicita nota e comentário da\navaliação
activate mb
deactivate mb
deactivate mb

Membro -> mb : 8. entra nota e comentário da avaliação
activate mb
mb -> afc: avaliar(filmeEscolhido, nota, comentario)
activate afc
create "__aval:Avaliacao__" as avaliacao
afc --> avaliacao : <<create>> new Avaliacao(filmeEscolhido, nota, comentario)
afc -> ac : salvar(aval)
activate ac
deactivate ac
deactivate afc
mb -> mb: 9. informa que avaliação foi registrada
activate mb
deactivate mb
deactivate mb
@enduml
```

### 4.4. Obter recomendações (CDU004)
(elaborar)

## 5. Diagramas de classes de projeto
